//  Google geo- Autocomplete
function initialize() {
            var input = document.getElementById('loc');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', display)
            function display(){
                var place = autocomplete.getPlace();
                document.getElementById("Lat").value = place.geometry.location.lat();
                document.getElementById("Lng").value = place.geometry.location.lng();
            }
        }
        google.maps.event.addDomListener(window, 'load', initialize); 

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } 
    else { 
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    
    codeLatLng(lat, lng)
        
}
function codeLatLng(lat, lng) {

    geocoder = new google.maps.Geocoder();
    
    var latlng = new google.maps.LatLng(lat, lng);
    
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    document.getElementById("loc").value =results[0].formatted_address ;
                } 
                else {
                  alert("No results found");
                }
        } 
        else {
            alert("Geocoder failed due to: " + status);
        }
    });
}

//      Signup Form Validation
function validate(){
    if (document.getElementById("signup").elements[5].value.length < 6){
		alert("Password : Minimum 6 characters");
		return false;
	}
	else if (document.getElementById("signup").elements[5].value != document.getElementById("signup").elements[6].value){
		alert("Passwords doesn't match");
		return false;
	}
	else if (!document.getElementById("signup").elements[13].checked){
		alert("Please agree to our T&C");
		return false;
	}
}