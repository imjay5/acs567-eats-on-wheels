// create a new express routes
const express               = require('express'),
    router                  = express.Router(),
    bodyParser              = require('body-parser'),
    mainController          = require('./controllers/main.controller'),
    signupController        = require('./controllers/signup.controller'),
    restaurantsController   = require('./controllers/restaurants.controller'),
    checkoutController      = require('./controllers/checkout.controller'),
    payController           = require('./controllers/pay.controller'),
    adminController         = require('./controllers/admin.controller'),
    sessionController       = require('./controllers/session.controller'),
    profileController       = require('./controllers/profile.controller'),
    blockController         = require('./controllers/block.controller'),
    feedbackController      = require('./controllers/feedback.controller'),
    blogController          = require('./controllers/blog.controller');
    
    var urlencodedParser = bodyParser.urlencoded({ extended: false });
    
    
//  export reroutes
    module.exports = router;

// define routes  

//  main routes
    router.get('/', mainController.showHome);


//  signup and login routes
    router.get('/signup', signupController.showForm);
    router.get('/login', signupController.showLogin);
    router.post('/login', urlencodedParser, signupController.login);
    router.post('/success', urlencodedParser, signupController.signUp);

//  profile page
    router.get('/profile', profileController.viewProfile)
    router.get('/editProfile', profileController.editProfile)
    router.post('/editSubmit', urlencodedParser, profileController.updateProfile)

//  blog page
    router.get('/blog', blogController.seeBlog)
    router.post('/blog', urlencodedParser, blogController.sendPost)

//  restaurants route
    router.post('/restaurants', urlencodedParser, restaurantsController.showRestaurants)

//  menu route
    router.get('/restaurants/menu/:name',restaurantsController.showMenu);
    
//  guest route
    router.post('/guestUser', urlencodedParser, sessionController.getGuestEmail)

//  checkout route
    router.post('/checkout', urlencodedParser, checkoutController.checkout )

//  Payment Page route
    router.get('/pay',payController.showPayPage);

//  logout
    router.get('/logout',sessionController.logout);

//  Admin Page
    router.get('/admin',adminController.showAdmin);
    
//  Quality dashboard
    router.get('/quality',adminController.showQuality);
    
//  Sales dashboard
    router.get('/sales',adminController.showSales); 
    
//  Adminteam dashboard
    router.get('/adminteam',adminController.showAdminteam);
    router.post('/adminteam', urlencodedParser, adminController.searchTable);

//  Adminteam dashboard - block user    
    router.post('/adminteam/block', urlencodedParser, blockController.blockUser);

//  feedback form
    router.get('/feedback',feedbackController.showFeedback);
    router.post('/sendFeedback',urlencodedParser,feedbackController.sendFeedback);
    