const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create a schema
const visitSchema = new Schema({
    visitcount: {type: Number, default:0 }
});

// create the model
const visitModel = mongoose.model('Visit', visitSchema);

// export the model
module.exports = visitModel;