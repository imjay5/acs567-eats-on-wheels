const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create a schema
const orderSchema = new Schema({
  orderId : Number,
  username: String,
  city : String,
  restaurant : String,
  cartArray : [],
  orderAmount : Number,
  orderDate : Date
});

// create the model
const orderModel = mongoose.model('Order', orderSchema);

// export the model
module.exports = orderModel;