
const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create visitdetail schema
const visitdetailSchema = new Schema({
    IP: String,
    Date: Date.now()
});

// create detail visit model
const visitdetailModel = mongoose.model('Visitdetail', visitdetailSchema);

// export detail visit model
module.exports = visitdetailModel;