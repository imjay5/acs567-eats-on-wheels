const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create a schema
const feedbackSchema = new Schema({
    username: String,
    rating: Number,
    comments: String,
    recommend: String

});

// create the model
const feedbackModel = mongoose.model('Feedback', feedbackSchema);

// export the model
module.exports = feedbackModel;