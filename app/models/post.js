const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create a schema
const postSchema = new Schema({
    postdata: String,
    username : String
});

// create the model
const postModel = mongoose.model('Post', postSchema);

// export the model
module.exports = postModel;