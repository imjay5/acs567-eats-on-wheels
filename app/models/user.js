const mongoose = require('mongoose'),
  Schema = mongoose.Schema; 
 
// create a schema
const userSchema = new Schema({
    fname: String,
    lname: String,
    username: String,
    phone: Number,
    email: String,
    password: String,
    streetAddr: String,
    city: String,
    state: String,
    country: String,
    zipcode: Number,
    source:  String,
    usertype : {type: String, default: "user"}
});

// create the model
const userModel = mongoose.model('User', userSchema);

// export the model
module.exports = userModel;