const User = require('../models/user');
module.exports = {
    
    blockUser : (req, res) => {
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
       
        var toblock = req.body.blockuser;
        console.log(req.body.blockuser);
        
        //find th user to block
        User.find({username : toblock}, function(err, result) {
            if (err) throw err;
            console.log(result);
            var userType = { $set: {usertype : 'blocked-user'} };
            var query = {username : toblock};
           
           // Update the usertype from user to blocked user
            User.update(query, userType, function(err, res) {
            if (err) throw err;
            console.log(res);
            })
        });
        
        // display all the records without blocked users/admin users
        User.find({usertype : 'user'}, function(err, doc) {
        if (err) throw err;
        console.log(doc);
        res.render('pages/adminteam', { success : success, username : username, userdata : doc, guestCount : req.session.guestCount, regCount : req.session.regCount, blockedCount : req.session.regCount } );
        });    
    }
};