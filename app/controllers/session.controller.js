const User = require('../models/user');

module.exports = {
  logout : logout,
  getGuestEmail : getGuestEmail

}

  function logout (req, res) {
    req.session.destroy()
    res.render('pages/home', {success : false, username : '' });
    
  }
  
  function getGuestEmail (req, res) {
    
      var query = { "email" : req.body.email };
      
       User.findOne(query, function(err, result) {
         if (err) throw err;
         console.log(result)
         if(result){
                 res.render('pages/home', { success : 'existing', username : null } )
         } else {
           var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
      
      var randomGuestId = 'guest' +Math.floor((Math.random() * 1000) + 5);
    
      var gUser = {fname: 'Guest',   username: randomGuestId,  phone: req.body.phone,  email: req.body.email,   address: req.body.address, usertype : 'guest'}
      var guest = new User(gUser);
      guest.save();
    
      req.session.username = randomGuestId;
      req.session.useremail = req.body.email;
      console.log(req.session.useremail)
      req.session.status = 'logged in';
    
      res.render('pages/home', { success : true, username : randomGuestId } )
           
         }
       });
    
      
  }
  
