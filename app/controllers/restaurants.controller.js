module.exports = { 
  showRestaurants : showRestaurants ,
  showMenu : showMenu
}

const https  = require('https')
    
function showRestaurants (req, res) {
      const apiKey = "AIzaSyDFLltzbl_pHCJfkv-ZQWQ-QGgs8Er62Oc";
      const radius = 500;
      const url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+"+ req.body.loc + "&key=" + apiKey;
      
      var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
          req.session.city = req.body.loc;
      }
      
      https.get(url, (response) => {
        let result = ''
          response.on('data', (chunk) => { 
          result += chunk
        })
        
        response.on('end', () => {
            const parsedData = JSON.parse(result)
            res.render('pages/restaurants', { restaurants: parsedData.results, success : success, username : username });
        })
      }).on('error', (error) => {
        console.error(`Got error: ${error.message}`)
      })
}

function showMenu (req, res) {
    
      var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
      }
      
      req.session.restaurant = req.params.name;
  
    const menu = {"items": [
                            {
                                "name":"Coke",
                                "price": 1,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Pepsi",
                                "price": 1,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Burger",
                                "price": 5,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Granola",
                                "price": 5,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Oatmeal",
                                "price": 7,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Side Salad  ",
                                "price": 2,
                                "qty":1,
                                "sizes":["small","large"]
                            },
                            {
                                "name":"Milk Shake",
                                "price": 3,
                                "qty":1,
                                "sizes":["small","large"]
                            }
                          ]
                      };
        
        res.render('pages/menu', {items: menu.items , username : req.session.username, success : success});
    }
