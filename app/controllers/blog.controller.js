const Post = require('../models/post');

module.exports = { 
  seeBlog : seeBlog,
  sendPost : sendPost
}

function seeBlog (req, res){
     var success = false;
    if(req.session.status == 'logged in'){
      success = true; 
      var username = req.session.username;
    }
    
    Post.find({},function(err, result) {
          if (err) throw err;
          
          res.render('pages/blog', { success : success, username : username, posts : result })
          
    });
    
}

function sendPost (req, res){
     var success = false;
    if(req.session.status == 'logged in'){
      success = true; 
      var username = req.session.username;
    }
    
    if(req.session.username == null){
       var username = 'Anonymous'
    }
    
    var newPost = new Post({postdata :req.body.postdata, username :username  });
    newPost.save();

    
    Post.find({},function(err, result) {
          if (err) throw err;
          console.log(result)

          result.push({ postdata: req.body.postdata, username: username})
          res.render('pages/blog', { success : success, username : username, posts : result })
          
    });
}