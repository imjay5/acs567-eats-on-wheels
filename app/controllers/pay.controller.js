var nodemailer = require('nodemailer');
var moment = require('moment-timezone');

const Order = require('../models/order');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'eats.on.wheels1@gmail.com',
    pass: 'IPFWSpring2018'
  }
}); 

module.exports = {
  showPayPage : showPayPage

}

  // show the pay page 
  function showPayPage (req, res) {
    var total = 0;
    for(var i = 0; i<req.session.cartItems.length; i++){
        total = total + (req.session.cartItems[i].itemPrice * req.session.cartItems[i].itemQty)
    }
    var orderId = Math.floor((Math.random() * 100000) + 5);
    console.log(req.session.city);
    // var orderDate = new Date().toISOString();
    var orderDate = moment(new Date());
    var formattedDate =orderDate.tz('America/Indianapolis').format()
    console.log(formattedDate)
    
    var order = {orderId : orderId, city : req.session.city, username : req.session.username, restaurant : req.session.restaurant, 
                 cartArray : req.session.cartItems, orderAmount : total, orderDate : formattedDate}
    var newOrder = new Order(order);
      newOrder.save();
    
    var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
    
    res.render('pages/pay', { success : success, username : username } );
    
    var mailOptions = {
      from: 'eats.on.wheels1@gmail.com',
      to: req.session.useremail,
      subject: 'Your recent order from Eats on Wheels',
      text: 'Your payment is successful'
    };
  
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }
  
