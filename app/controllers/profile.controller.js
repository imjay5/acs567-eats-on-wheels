var moment = require('moment-timezone');

const User = require('../models/user');
const Order = require('../models/order');

module.exports = {
  viewProfile : viewProfile,
  editProfile : editProfile,
  updateProfile : updateProfile
}

function viewProfile (req, res) {
    
    var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
      
      var query = { "username" : req.session.username };
      User.find(query, function(err, result) {
          if (err) throw err;
          console.log(result[0]);
          var formattedDates = []
          Order.find(query, function(err, ordersResult){
            if (err) throw err;
            for(var i = 0; i<ordersResult.length; i++){
              var orderDate = moment(ordersResult[i].orderDate);
              var formattedDate =orderDate.tz('America/Indianapolis').format()
              formattedDates.push(formattedDate)
            }
            
            console.log(formattedDates)
            
            res.render('pages/profile',  { success : success, username : username, userDetails : result[0], orders : ordersResult, formattedDates : formattedDates })
            
            
            // console.log(ordersResult);
          });
          
          
      });
      
}

function editProfile (req, res) {
      var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
      
      var query = { "username" : req.session.username };
      User.find(query, function(err, result) {
        if (err) throw err;
        
              res.render('pages/edit', { success : success, username : username, userDetails : result[0]} )
      });
        
}


function updateProfile (req, res) {
      var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
      
      var query = { "username" : req.body.username };

      User.findOne(query, function(err, result) {
        if (err) throw err;
          result.fname = req.body.fname;
          result.phone = req.body.phone;
          result.password = req.body.password;
          result.streetAddr = req.body.streetAddr;
          result.usertype = "user";
          result.city = req.body.city;
          result.state = req.body.state;
          result.country = req.body.country;
          result.zipcode = req.body.zipcode;
          
          result.save((err) => {
            if (err) throw err 
          })
          
          Order.find(query, function(err, ordersResult){
            if (err) throw err;
            res.render('pages/profile',  { success : success, username : username, userDetails : req.body, orders : ordersResult })
          });

      });
      
        
}