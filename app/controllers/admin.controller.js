const Visit = require('../models/visit');
const User = require('../models/user');
const Feedback = require('../models/feedback');
const Order = require('../models/order');

module.exports = {
    
    showAdmin : (req, res) => {
        var visitcount = 0;
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
        Visit.find({}, function(err, result) {
            if (err) throw err;
            visitcount = result[0].visitcount;
            
            var avgRating = req.session.avgRating;
            var regCount = req.session.userCount;
            var totalSales= req.session.totalSales;
            
            
            if(req.session.usertype == "admin")
              res.render('pages/admin', { success : success, username : username, visitcount: visitcount, avgRating : avgRating, regCount : regCount, totalSales : totalSales} );
            else
              res.render('pages/noauth', { success : success, username : username } );
                        
        });
        
    },
    
//  Includes all the functions of the Quality dashboard
    showQuality : (req, res) => {
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
        Feedback.find({}, function(err, result) {
            if (err) throw err;
            console.log(result);
            Feedback.aggregate([{$group: {_id: '$recommend', count: {$sum: 1}}}, {$sort : {"count": -1}}], function (err, recommendData) {
                if (err) throw err;
                console.log(recommendData);
            
                Feedback.aggregate([{$group: {_id: '$rating', count: {$sum: 1}}}, {$sort : {"count": -1}}], function (err, ratingData) {
                    if (err) throw err;
                    console.log(ratingData);
                    
                    Feedback.find({}, function (err, rating) {
                        var totalRating = 0;
                        if (err) throw err;
                        for(var r = 0; r< rating.length; r++){
                            totalRating=rating[r].rating;
                        }
                        
                        var averageRating = (totalRating/rating.length);
                        averageRating = req.session.avgRating 
                        
                        if(req.session.usertype == "admin")
                            res.render('pages/quality', { success : success, username : username, recommendData : recommendData, ratingData : ratingData, averageRating:averageRating  } );
                        else
                            res.render('pages/noauth', { success : success, username : username } );
                        
                    });
                    
                });
            });
        });
    },

//  Includes all the functions of the sales dashboard
    showSales : (req, res) => {
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
          
        Order.find({}, function (err, orders) {
            if (err) throw err
        
        if(req.session.usertype == "admin")
            res.render('pages/sales', { success : success, username : username, orders : orders} ); 
        else
            res.render('pages/noauth', { success : success, username : username } );  
            
        });
    },

//  Includes all the functions of the Admin dashboard    
    showAdminteam : (req, res) => {
        var success = false; 
        var userdata;
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
        User.find({}, function(err, result) {
            
            if (err) throw err;
            
            var regCount=0;
            var guestCount=0;
            var blockedCount=0;
            var userArray = [];        
            
            for(var i = 0; i< result.length; i++){
                if(result[i].usertype == 'user'){
                    userArray.push(result[i])
                    regCount++;
                }
                else if(result[i].usertype == 'guest')
                    guestCount++;
                else
                    blockedCount++;
            }
            
            req.session.regCount = regCount;
            req.session.guestCount = guestCount;
            req.session.blockedCount = blockedCount;
            req.session.userCount = regCount + guestCount;
            
            if(req.session.usertype == "admin")
                res.render('pages/adminteam', { success : success, username : username, userdata: userArray, regCount : regCount, guestCount : guestCount, blockedCount : blockedCount} );
            else
                res.render('pages/noauth', { success : success, username : username } );
        });
    },
    
    searchTable : (req, res) => {
        var success = false; 
        var userdata;
        var searchval= req.body;
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
        
        User.find(searchval, function(err, result) {
        if (err) throw err;
            var regCount=0;
            var guestCount=0;
            var blockedCount=0;
            regCount = req.session.regCount
            guestCount = req.session.guestCount;
            blockedCount =  req.session.blockedCount;
        res.render('pages/adminteam', { success : success, username : username, userdata: result, regCount : regCount, guestCount : guestCount, blockedCount : blockedCount} );
        });
    }
};

