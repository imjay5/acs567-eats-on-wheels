const User = require('../models/user');
const Visit = require('../models/visit');
var nodemailer = require('nodemailer');
const Order = require('../models/order');
const Feedback = require('../models/feedback');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'eats.on.wheels1@gmail.com',
    pass: 'IPFWSpring2018'
  }
}); 


module.exports = {

  // show the home page 
  showForm: (req, res) => {
    var success = false;
        if(req.session.status == 'logged in'){
            success = true; 
    }
    res.render('pages/register', {success : success});
  },
  
  
  signUp: (req, res) => {
    
    var query = { "email" : req.body.email };
    var query1 = { "username" : req.body.username };
    
       User.findOne(query1, function(err, result) {
         if (err) throw err;
         console.log(result)
         if(result){
                 res.render('pages/register', { success : 'existing', username : null } )
         } else {
           User.findOne(query, function(err, result) {
         if (err) throw err;
         console.log(result)
         if(result){
                 res.render('pages/home', { success : 'existing', username : null } )
         } else {
           
          // console.log(req.body);
            const user = req.body;
            var newUser = new User(user);
            newUser.save();
            
            var mailOptions = {
            from: 'eats.on.wheels1@gmail.com',
            to: req.body.email,
            subject: 'Welcome ' + req.body.fname,
            html: '<h1> We are excited to have you join Us ...</h1><h3>Hope you have a great experience</h3><h5>Best Regards, <br>Cheers! ,<br>Team Eats-on-Wheels</h5>'
          };
        
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
          
            req.session.username = req.body.username;
            req.session.status = 'logged in';
          res.render('pages/home',{username : req.session.username, success : 'registered'});
           
           
         }});
    
           
         }
       });
  },
  
    showLogin: (req, res) => {
      var success = false;
        if(req.session.status == 'logged in'){
            success = true; 
      }
      res.render('pages/login', { success : success } );
  },
  
  login: (req, res) => {
      console.log(req.body);
      const user = req.body;
      var query = { "username" : req.body.username, "password" : req.body.password };
      
      User.find(query, function(err, result) {
        if (err) throw err;
        
        console.log(result.length)
        if(result.length > 0){
            req.session.username = req.body.username;
            req.session.usertype = result[0].usertype;
            req.session.useremail = result[0].email;
            console.log(req.session.useremail)
            req.session.status = 'logged in';
            
            
            Visit.find({}, function(err, rs) {
            if (err) throw err;
            var visitcount = rs[0].visitcount;
            
            var regCount = req.session.userCount;
            var avgRating = req.session.avgRating
              
               if(result[0].usertype == "admin"){
                 Order.find({}, function(err, sales) {
                   if (err) throw err;
                   var totalSales = 0;
                   for(var i= 0; i<sales.length; i++){
                     totalSales = totalSales + sales[i].orderAmount
                   }
                   req.session.totalSales = totalSales;
                   
                   Feedback.find({}, function(err, rating) {
                   if (err) throw err;
                   var avgRating = 0, totalRating = 0;
                   for(var i= 0; i<rating.length; i++){
                     totalRating = totalRating + rating[i].rating
                   }
                   avgRating = totalRating/(rating.length)
                   req.session.avgRating = avgRating;
                   
                   User.find({}, function(err, users){
                     if (err) throw err;
                     var userCount = 0
                     for(var i=0; i<users.length; i++){
                       if(users[i].usertype == "user" || users[i].usertype == "guest"){
                         userCount++;
                       }
                     }
                     req.session.userCount = userCount;
                     res.render('pages/admin', {username : req.session.username, success : 'logged in', visitcount: visitcount, avgRating : avgRating, regCount : userCount, totalSales : totalSales} );

                   });
                   
                   
                   
                   });
                 }); 
               
               
               }
               else if(result[0].usertype == "blocked-user")
                  res.render('pages/home', {username : null, success : 'blocked'} );
               else
                  res.render('pages/home', {username : req.session.username, success : 'logged in'} );
            });
            
        } else{
              res.render('pages/home', {username : null, success : 'invalid'} );
        }
      
      
      });
      
      
  }
  
};