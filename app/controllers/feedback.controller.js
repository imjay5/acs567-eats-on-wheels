const Feedback = require('../models/feedback');

module.exports = {
  showFeedback : showFeedback,
  sendFeedback : sendFeedback
}

function showFeedback (req, res){
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
      res.render('pages/feedback', { success : success, username : username} ); 
}

function sendFeedback (req, res){
        var success = false;    
        if(req.session.status == 'logged in'){
            success = true; 
            var username = req.session.username;
          }
          var feedback = { "username" : req.session.username, "rating": req.body.rating, "comments":req.body.comments, "recommend" : req.body.recommend};
          
          var newFeedback = new Feedback(feedback);
          newFeedback.save();
          
            Feedback.find({}, function(err, result) {
            console.log(result)
            Feedback.aggregate([{$group: {_id: '$recommend', count: {$sum: 1}}}], function (err, doc) {
                if (err) throw err;
                console.log(doc);
            });

           res.render('pages/home', { success : success, username : username} );  
            
          })
       
}

//db.feedback.aggregate([{$group: {_id: '$recommend', count: {$sum: 1}}}])

//{ $group: { _id: null, <field1>: { <accumulator1> : <expression1> }, ... } }

