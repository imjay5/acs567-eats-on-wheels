const Visit = require('../models/visit');
module.exports = {

  // show the home page 
  showHome: (req, res) => {
   
    //setting the login succes variable depending on status
    var success = false;
    if(req.session.status == 'logged in'){
      success = true; 
      var username = req.session.username;
    }
    //Finding the last updated count      
    Visit.find({}, function(err, result) {
      if (err) throw err;
      //incrementing the count by one ==> denoting a new visit
      var newVisitCount = result[0].visitcount + 1;
      
      req.session.visitCount = newVisitCount;
      
      //updating the old value in the db with the incremented value
      var newvalues = { $set: {visitcount : newVisitCount} };
      Visit.updateOne({},newvalues, function(err, res) {
          if (err) throw err; 
        })
    })
    res.render('pages/home', { success : success, username : username } );
  }

};