module.exports = { 
  checkout : checkout
}

function checkout (req, res){
    
    var success = false;
      if(req.session.status == 'logged in'){
          success = true; 
          var username = req.session.username;
      }
    
    
    var jsonStr = '{"items":[]}';
    var obj = JSON.parse(jsonStr);
    
    for(var i = 0; i < req.body.itemName.length; i++){
        if(req.body.itemQty[i] > 0)
            obj['items'].push({"itemName":req.body.itemName[i],"itemPrice":req.body.itemPrice[i] ,"itemQty":req.body.itemQty[i]});
    }
    
    req.session.cartItems = obj.items;
    // console.log(req.session.cartItems)
    // console.log(req.session.restaurant);
    
    if(obj.items.length == 0){
        res.redirect('/restaurants/menu/'+ req.session.restaurant)
    } else {
        res.render('pages/checkout', { cartItems: obj.items, success : success, username : username })
    }
    
}