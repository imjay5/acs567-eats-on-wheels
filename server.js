require('dotenv').config();

// grab our dependencies
const express = require('express')
var app = express()
var port = process.env.PORT || 8080
var expressLayouts = require('express-ejs-layouts')
var mongoose = require('mongoose');
var session = require('express-session')
var cookieParser = require('cookie-parser');
var moment = require('moment-timezone');

  
// configure our application
// tell express where to look for static assets
app.use(express.static(__dirname + '/public'));

// set ejs as our templating engine
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(cookieParser());
app.use(session({ secret: 'random_string_goes_here',
                  resave: true,
                  saveUninitialized: true
                })
);

// connect to our database
mongoose.connect(process.env.DB_URI);

// set the routes
app.use(require('./app/route'));

// start our server 
app.listen(port, () => {
  console.log(`App listening on http://localhost:${port}`);
});